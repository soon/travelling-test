from django.contrib import admin
from django.urls import path
from django.views.generic import RedirectView

from travelling.views import VehicleCreateView, VehicleUpdateView, JourneyCreateView, JourneyUpdateView, \
    VehicleListView, JourneyListView, PassengersStatsView


urlpatterns = [
    path('admin/', admin.site.urls),

    path('', RedirectView.as_view(url='journeys')),
    path('vehicles/', VehicleListView.as_view(), name='vehicles'),
    path('vehicles/new/', VehicleCreateView.as_view(), name='vehicles--new'),
    path('vehicles/<int:pk>/', VehicleUpdateView.as_view(), name='vehicles--by-pk'),
    path('journeys/', JourneyListView.as_view(), name='journeys'),
    path('journeys/new/', JourneyCreateView.as_view(), name='journeys--new'),
    path('journeys/<int:pk>/', JourneyUpdateView.as_view(), name='journeys--by-pk'),
    path('passengers-stats/', PassengersStatsView.as_view(), name='passengers-stats'),
]
