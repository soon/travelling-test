from django import forms
from django.db.models import QuerySet

from travelling.models import Journey


class JourneyForm(forms.ModelForm):
    started_at = forms.DateTimeField(
        widget=forms.widgets.DateTimeInput(attrs={'type': 'datetime-local'},
                                           format='%Y-%m-%dT%H:%M:%S'),
        input_formats=['%Y-%m-%dT%H:%M', '%Y-%m-%dT%H:%M:%S', '%Y-%m-%d %H:%M:%S'])
    km_length = forms.FloatField(min_value=0.1)

    class Meta:
        model = Journey
        fields = '__all__'

    def clean_km_length(self):
        km_length = round(self.cleaned_data['km_length'], 1)
        return km_length

    def clean_passengers(self):
        passengers: QuerySet = self.cleaned_data['passengers']
        return passengers.exclude(pk=self.cleaned_data['driver'].pk)
