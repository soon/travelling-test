from django.contrib import admin

from travelling.models import Vehicle, Journey


@admin.register(Vehicle)
class VehicleAdmin(admin.ModelAdmin):
    pass


@admin.register(Journey)
class JourneyAdmin(admin.ModelAdmin):
    pass
