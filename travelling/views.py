from collections import defaultdict
from typing import Tuple, DefaultDict

from django.contrib.auth.models import User
from django.db.models import Sum
from django.views.generic import CreateView, UpdateView, ListView, TemplateView

from travelling.forms import JourneyForm
from travelling.models import Vehicle, Journey


class VehicleListView(ListView):
    model = Vehicle

    def get_queryset(self):
        qs = super().get_queryset()
        return qs.annotate(total_km_length=Sum('journey__km_length'))


class VehicleCreateView(CreateView):
    model = Vehicle
    fields = '__all__'


class VehicleUpdateView(UpdateView):
    model = Vehicle
    fields = '__all__'


class JourneyListView(ListView):
    model = Journey


class JourneyCreateView(CreateView):
    model = Journey
    form_class = JourneyForm


class JourneyUpdateView(UpdateView):
    model = Journey
    form_class = JourneyForm


class PassengersStatsView(TemplateView):
    template_name = 'travelling/passengers_stats.html'

    def get_context_data(self, **kwargs):
        pk_to_user = {x.pk: x for x in User.objects.all()}
        pk_to_vehicle = {x.pk: x for x in Vehicle.objects.all()}
        passenger_stats = User.objects.values('pk', 'journeys_as_passenger__vehicle').annotate(
            total_km=Sum('journeys_as_passenger__km_length'))
        driver_stats = User.objects.values('pk', 'journeys_as_driver__vehicle').annotate(
            total_km=Sum('journeys_as_driver__km_length'))

        user_vehicle_ids_to_total_km: DefaultDict[Tuple[int, int], float] = defaultdict(float)
        for s in passenger_stats:
            user_vehicle_ids_to_total_km[s['pk'], s['journeys_as_passenger__vehicle']] = s['total_km']

        for s in driver_stats:
            user_vehicle_ids_to_total_km[s['pk'], s['journeys_as_driver__vehicle']] += s['total_km']

        users = sorted(pk_to_user.values(), key=lambda x: x.username)
        vehicles = sorted(pk_to_vehicle.values(), key=lambda x: x.registration_number)

        columns = ['Vehicle'] + [x.username for x in users]
        rows = []
        for v in vehicles:
            row = [v.registration_number]
            for u in users:
                row.append(user_vehicle_ids_to_total_km[(u.pk, v.pk)])
            rows.append(row)

        kwargs.setdefault('stats_table', {
            'columns': columns,
            'rows': rows
        })

        return super().get_context_data(**kwargs)
