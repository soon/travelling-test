from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse


class Vehicle(models.Model):
    registration_number = models.TextField(unique=True)

    def __str__(self):
        return self.registration_number

    def get_absolute_url(self):
        if self.pk is None:
            raise ValueError('Object must be saved')
        return reverse('vehicles--by-pk', kwargs={'pk': self.pk})


class Journey(models.Model):
    vehicle = models.ForeignKey(Vehicle, on_delete=models.CASCADE)
    started_at = models.DateTimeField()
    km_length = models.FloatField()
    driver = models.ForeignKey(User, on_delete=models.CASCADE, related_name='journeys_as_driver')
    passengers = models.ManyToManyField(User, related_name='journeys_as_passenger', blank=True)

    def get_absolute_url(self):
        if self.pk is None:
            raise ValueError('Object must be saved')
        return reverse('journeys--by-pk', kwargs={'pk': self.pk})

    @property
    def passengers_display(self):
        return ', '.join(map(str, self.passengers.all()))
